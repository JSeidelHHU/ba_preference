scenario = "preference-passiv"; 

response_matching = simple_matching;
active_buttons = 3;
button_codes = 1,2,3;
target_button_codes = 101,102,103;
randomize_trials = true;
#write_codes = true;
pulse_width = 50;

begin;

#feste Variable für den eigentlichen Lerntrial 
$patterntime=3500; #Lern-Muster


# feste Variable für no-response Feedback Trial
$respfeedtime=300;
$black1time=500;
$startrew='$black1time+$respfeedtime';

$rewtime=500;

$rewtime_2=1000;# für no-response

$startfix2='$startrew+$rewtime';

$black1plusrew_2='$black1time+$rewtime_2'; # für no response-feedback - Summe aus fixtime und rewtime 

# Variablen für test-feedback trials
$fixtime=500;
#$respfeedtime=500; # Achtung - diese Variable wird noch einmal definiert für die Lern-Feedback-Trials
$blacktest=300;

$folder = EXPARAM("instrfolder");

bitmap { filename = "$folder//Instruktionen//folie1.png"; } instr1;
bitmap { filename = "$folder//Instruktionen//folie2.png"; } instr2;
bitmap { filename = "$folder//Instruktionen//folie3.png"; } instr3;
bitmap { filename = "$folder//Instruktionen//folie4.png"; } instr4;
bitmap { filename = "$folder//Instruktionen//folie5.png"; } instr5;
bitmap { filename = "$folder//Instruktionen//folie6.png"; } instr6;
bitmap { filename = "$folder//Instruktionen//folie7.png"; } testblockinstr;
bitmap { filename = "$folder//Instruktionen//folie8.png"; } xxx;
bitmap { filename = "$folder//Instruktionen//folie9.png"; } lernblockstart;
bitmap { filename = "$folder//Instruktionen//folie10.png"; } testblockstart;
bitmap { filename = "$folder//Instruktionen//folie11.png"; } ende;
bitmap { filename = "$folder//Instruktionen//folie12.png"; } instr12;
bitmap { filename = "$folder//Instruktionen//folie13.png"; } instr13;
bitmap { filename = "$folder//Instruktionen//folie14.png"; } pause;
bitmap { filename = "$folder//Instruktionen//folie15.png"; } corona;
bitmap { filename = "$folder//Instruktionen//folie16.png"; } instr16;

	text {caption = "name1";  font_color= 255, 0, 0; } activeText; 
	text {caption = "name2";  font_color= 0, 0, 255; } passiveText; 

	text {caption = "schoko"; } schokotext1; 
	text {caption = "schoko"; } schokotext2; 
	text {caption = "schoko"; } schokotext3; 
	text {caption = "schoko"; } schokotext4; 
	text {caption = "schoko"; } schokotext5; 
	text {caption = "schoko"; } schokotext6; 


bitmap { filename = "Ende_Uebung.bmp"; } endeuebung;
bitmap { filename = "Ende.bmp"; } Ende;
#neu


bitmap { filename = "p1_1.bmp"; } p1;
bitmap { filename = "p2_1.bmp"; } p2;
bitmap { filename = "p3_1.bmp"; } p3;
bitmap { filename = "p4_1.bmp"; } p4;
bitmap { filename = "p5_1.bmp"; } p5;
bitmap { filename = "p6_1.bmp"; } p6;


bitmap { filename = "p1_1_ch.bmp"; } p1_ch;
bitmap { filename = "p2_1_ch.bmp"; } p2_ch;
bitmap { filename = "p3_1_ch.bmp"; } p3_ch;
bitmap { filename = "p4_1_ch.bmp"; } p4_ch;
bitmap { filename = "p5_1_ch.bmp"; } p5_ch;
bitmap { filename = "p6_1_ch.bmp"; } p6_ch;

bitmap { filename = "black_screen.bmp"; } black_screen;
bitmap { filename = "black_screen_small.bmp"; } black_screen_small;
bitmap { filename = "fix.bmp"; } fix; 

bitmap { filename = "hell.jpg"; } hell;
bitmap { filename = "dunkel.jpg"; } dunkel;
bitmap { filename = "Esspapier.jpg"; } Esspapier;

bitmap { filename = "no_reward_2.bmp"; } no_resp_no_rew;

text {caption="0";font_size=10;}block_trial;
	
trial {	
	trial_duration = forever;	
	trial_type = specific_response;
	terminator_button = 3;
	
	picture {
		bitmap instr13; 
		x=0; y=0;
		text schokotext1;
		x = -40; y = 45;
		text schokotext2;
		x = -40; y = -15;
		text schokotext3;
		x = -40; y = -75;
	};
	time=0;
	duration=1000000;
	
} Report;

#-------------------------------------------------------------------------------------------------
# start learn session - dieser Trial ist wichtig für fMRI
#---------------------------------------------------------


trial {
		trial_duration = stimuli_length;
		
		stimulus_event {
		picture {						  
		bitmap fix;
		x = 0; y = 0;};
		time = 0;
		duration = 1000;
		code = "fixlearn_start_uebung";}start_uebung_event;}start_uebung;


trial {
		trial_duration = stimuli_length;
		
		stimulus_event {
		picture {						  
		bitmap fix;
		x = 0; y = 0;};
		time = 0;
		duration = 1000;
		code = "fixlearn_start_learn_session";}start_learn_session_event;}start_learn_session;


#-------------------------------------------------------------------

#feedback trials - aktiv

#feedback keine Taste gedrückt

trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
		
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	time = 0;
	duration = next_picture;
	code = "black1";	

	picture {
		bitmap no_resp_no_rew;
		x = 0; y = 0;};
	deltat = $black1time;
	duration = next_picture;
	code = "no_resp_no_rew";
	port_code = 248;
	
	stimulus_event {
	picture {
		bitmap fix;
		x = 0; y = 0;};
	deltat = $rewtime_2;
	duration = 2000;
	code = "fixlearn_no_resp";}fix_jitter1;}no_rew_time;   # no reward keine Taste gedrückt
	
	

#feedback Taste gedrückt - nur zwei Trials - Bilder werden z.T. in PCL zugewiesen

trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
	
	stimulus_event {
	picture {
		bitmap black_screen_small; #nur Platzhalter
		x = -200; y = 0;

		bitmap black_screen_small; #nur Platzhalter
		x = 200; y = 0;}choice_fb_r_pic1;
	time = 0;
	duration = next_picture;
	code = "choice_fb";}choice_fb_r_event1;
	
	stimulus_event {	
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	deltat = $respfeedtime;
	duration = next_picture;
	code = "black1";}black1_fb_r_event1;	

	stimulus_event {
	picture {
		bitmap dunkel; # nur Platzhalter
		x = 0; y = 0;}right_pic1;
	deltat = $black1time;
	duration = next_picture;
	code = "dunkel";
	port_code = 250;}right_event1;
	
	stimulus_event {
	picture {
		bitmap fix;
		x = 0; y = 0;};
	deltat = $rewtime;
	duration = 2000;
	code = "fixlearn_dunkel";}fix_jitter2;}feedback_r_dunkel; #korrekte Reaktion dunkle Schokolade


trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
	
	stimulus_event {
	picture {
		bitmap black_screen_small; #nur Platzhalter
		x = -200; y = 0;

		bitmap black_screen_small; #nur Platzhalter
		x = 200; y = 0;}choice_fb_r_pic2;
	time = 0;
	duration = next_picture;
	code = "choice_fb";}choice_fb_r_event2;
	
	stimulus_event {	
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	deltat = $respfeedtime;
	duration = next_picture;
	code = "black1";}black1_fb_r_event2;	

	stimulus_event {
	picture {
		bitmap hell; # nur Platzhalter
		x = 0; y = 0;}right_pic2;
	deltat = $black1time;
	duration = next_picture;
	code = "hell";
	port_code = 251;}right_event2;
	
	stimulus_event {
	picture {
		bitmap fix;
		x = 0; y = 0;};
	deltat = $rewtime;
	duration = 2000;
	code = "fixlearn_hell";}fix_jitter3;}feedback_r_hell; #korrekte Reaktion helle Schokolade


trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
	
	stimulus_event {
	picture {
		bitmap black_screen_small; #nur Platzhalter
		x = -200; y = 0;
		
		bitmap black_screen_small; #nur Platzhalter
		x = 200; y = 0;}choice_fb_f_pic;
	time = 0;
	duration = next_picture;
	code = "choice_fb";}choice_fb_f_event;
	
	
	stimulus_event {	
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	deltat = $respfeedtime;
	duration = next_picture;
	code = "black1";}black1_fb_f_event;		

	
	stimulus_event {
	picture {
		bitmap Esspapier; # nur Platzhalter
		x = 0; y = 0;}wrong_pic;
	deltat = $black1time;
	duration = next_picture;
	code = "Esspapier";
	port_code = 249;}wrong_event;
	
	stimulus_event {
	picture {
		bitmap fix;
		x = 0; y = 0;};
	deltat = $rewtime;
	duration = 2000;
	code = "fixlearn_Esspapier";}fix_jitter4;}feedback_f_Esspapier;   #falsche Reaktion

	
#------------------------------------------------------------------------------------------------
#Test Part


picture {
		bitmap black_screen_small;
		x = -200; y = 0;
		
		bitmap black_screen_small;
		x = 200; y = 0;}fb_c_test_picture;
	
# Feedback Test-Trial correct
trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
	
	stimulus_event {
	picture fb_c_test_picture;
	time = 0;
	duration = $respfeedtime;
	code = "testfeedback_c";}fb_c_test_event;
	
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	time= $respfeedtime;
	duration = $blacktest;
	code = "blacktest_c";}testfeedback_c;



picture {
		bitmap black_screen;
		x = -200; y = 0;
		
		bitmap black_screen;
		x = 200; y = 0;}fb_f_test_picture;
			
# Feedback Test-Trial falsch
trial{ #trial wird innerhalb der anderen Trials aufgerufen
	trial_duration = stimuli_length;
	
	stimulus_event {
	picture fb_f_test_picture;
	time = 0;
	duration = $respfeedtime;
	code = "testfeedback_f";}fb_f_test_event;
	
	picture {
		bitmap black_screen;
		x = 0; y = 0;};
	time= $respfeedtime;
	duration = $blacktest;
	code = "blacktest_f";}testfeedback_f;	
	


picture {						  
	bitmap black_screen_small;
	x = -200; y = 0;

	bitmap black_screen_small;
	x = 200; y = 0;}test_picture;


trial {	
	trial_duration = stimuli_length;
	all_responses = false; #ignorieren von zu frühen responses (kein Abbruch)
	trial_type = first_response; #Abbruch des Trials, wenn erster response kommt
										  #dann Start des feedback trials		
	
	no_response_feedback = no_rew_time;
	correct_feedback = testfeedback_c;
	incorrect_feedback = testfeedback_f;

	picture {						  
		bitmap fix;
		x = 0; y = 0;};
	time = 0;
	duration = $fixtime;
	code = "fixtest";
	
	stimulus_event {
	picture test_picture;
	time = $fixtime;
	duration = $patterntime;
	target_button=1;
	stimulus_time_in=0;
	stimulus_time_out='$patterntime+$blacktest';
	code = "test";}test_event;}test_trial; 


#-------------------------------------------------------------------------------------------------

# eigentliche Trials

# array für Instruktion
array {
	TEMPLATE "instruct.tem" {
		instpic j;
		instr1  1;
		corona;
		instr2  3;
		instr3  3;
		instr4  4;
		instr5  5;
		instr6  6;
		instr16;
		
		testblockinstr;
		xxx;
		lernblockstart;
		testblockstart;
		ende;
		instr12;		
		instr13;		
		pause;		

	};
}instr;




# Pause-Trials
trial {
		trial_duration = forever;
		trial_type = specific_response;
		terminator_button = 3;
		
		picture {						  
		bitmap pause;
		x = 0; y = 0;};
		time = 0;
		duration = 6000000;
		code = "pause";}pause_trial;
			

trial {
		trial_duration = forever;
		trial_type = specific_response;
		terminator_button = 3;
		
		picture {						  
		bitmap endeuebung;
		x = 0; y = 0;};
		time = 0;
		duration = 6000000;
		code = "endeuebung";}uebung;



trial {
		trial_duration = forever;
		trial_type = specific_response;
		terminator_button = 3;
		
		picture {						  
		bitmap Ende;
		x = 0; y = 0;};
		time = 0;
		duration = 6000000;
		code = "Ende";}end_trial;
	

picture {
		bitmap p1;
		x = -200; y = 0;
		bitmap p2;
		x = 200; y = 0;}p1p2;

picture {
		bitmap p2;
		x = -200; y = 0;
		bitmap p1;
		x = 200; y = 0;}p2p1;		

picture {
		bitmap p3;
		x = -200; y = 0;
		bitmap p4;
		x = 200; y = 0;}p3p4;

picture {
		bitmap p4;
		x = -200; y = 0;
		bitmap p3;
		x = 200; y = 0;}p4p3;		
		


array {
TEMPLATE "active_reward.tem" {
pic 	piccode 					t_button					rew_trial			no_rew_trial			no_resp_trial	pcode; 
p1p2 	"p1p2_p1r"						1					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		12;
p2p1 	"p2p1_p1r"						2					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		21;
p1p2 	"p1p2_p2r"						2					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		12;
p2p1 	"p2p1_p2r"						1					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		21;
};
}p1_p2;

array {
TEMPLATE "active_reward.tem" {
pic 	piccode 					t_button					rew_trial			no_rew_trial			no_resp_trial	pcode;
p3p4 	"p3p4_p3r"						1					feedback_r_hell 	feedback_f_Esspapier	no_rew_time		34;
p4p3 	"p4p3_p3r"						2					feedback_r_hell  	feedback_f_Esspapier	no_rew_time		43;
p3p4 	"p3p4_p4r"						2					feedback_r_hell 	feedback_f_Esspapier	no_rew_time		34;
p4p3 	"p4p3_p4r"						1					feedback_r_hell  	feedback_f_Esspapier	no_rew_time		43;
};
}p3_p4;


array {
TEMPLATE "passiv_reward.tem" {
pic 	piccode 					t_button					rew_trial			no_rew_trial			no_resp_trial	pcode; 
p1p2 	"p1p2_p1r"						1					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		12;
p2p1 	"p2p1_p1r"						2					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		21;
p1p2 	"p1p2_p2r"						2					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		12;
p2p1 	"p2p1_p2r"						1					feedback_r_dunkel feedback_f_Esspapier	no_rew_time		21;
};
}p1_p2_passiv;

array {
TEMPLATE "passiv_reward.tem" {
pic 	piccode 					t_button					rew_trial			no_rew_trial			no_resp_trial	pcode;
p3p4 	"p3p4_p3r"						1					feedback_r_hell 	feedback_f_Esspapier	no_rew_time		34;
p4p3 	"p4p3_p3r"						2					feedback_r_hell  	feedback_f_Esspapier	no_rew_time		43;
p3p4 	"p3p4_p4r"						2					feedback_r_hell 	feedback_f_Esspapier	no_rew_time		34;
p4p3 	"p4p3_p4r"						1					feedback_r_hell  	feedback_f_Esspapier	no_rew_time		43;
};
}p3_p4_passiv;




begin_pcl;
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
int presentinstruction=1; #wenn diese Parameter = 1 sind, soll die Instruktion bzw. die pause gezeigt werden
int presentpause=1;
int presentuebung=1;

int numrep=8; # 1 Übung, 4 Blöcke ohne FB 4

int numtrials_uebung=8;
int numtrials_block_1=60; #Anzahl Trials pro Lern-Block 60
int numtrials_block; 
int resp_pic;


int last_resp;
int last_resp2;

int last_rew;

string lastcode;
#-----------------------------------------------------------------------------------------
#----------------------------------------



# 1 - p1p2, 2 - p2p3, 3 - p3p4			  
array <int> patterns[60]={12,12,12,12,12,12,12,12,12,12,34,34,34,34,34,34,34,34,34,34, #vorkommende Muster-Paarungen 
								  34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,
								  12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12};							 

# relative Anzahl der Trialarten bestimmt die Verstärkungsrate für die Paarungen
array <int> trialtype_1[30]={1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,
									  2,2,2,2,2,2,2,2,2,2,4,4,4,4,4}; 
									  
array <int> trialtype_2[30]={1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,
									  2,2,2,2,2,2,2,2,2,2,4,4,4,4,4};
									  
array <int> jitter[60]={2000,2000,2000,2000,2000,2000,2000,2000, # es gibt pro Block 60 Lern_Trials, 
								2000,2000,2000,2000,2000,2000,2000,2000, # demzufolge 59 Inter-Trial-Intervalle
								2000,2000,2000,2000,1000,1000,1000,1000, 
								1000,1000,1000,1000,1000,1000,1000,1000, 
								1000,1000,1000,1000,1000,1000,1000,1000, 
								500,500,500,500,500,500,500,500,
								500,500,500,500,500,500,500,500,
								500,500,500,500};		

# folgendes array enthält die Zeiten für den schwarzen Bildschirm zwischen der eigenen Reaktion und dem Feedback
# da für 500ms die eigene Reaktion noch sichtbar ist (roter Kreis), muss man diese 500ms noch zu den untenstehenden Zeiten addieren,
# um die Länge des Intervalls zwischen Reaktion und Belohnung in einem Trial zu erhalten

					 

array <int> test_patterns[30]={12,12,12,12,12,12,12,12,12,12,34,34,34,34,34,34,34,34,34,34,56,56,56,56,56,56,56,56,56,56};
array <int> test_trialtype[30]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};

array <int> test_patterns_end[40]={13,13,13,13,13,14,14,14,14,14,15,15,15,15,15,16,16,16,16,16,23,23,23,23,23,24,24,24,24,24,25,25,25,25,25,26,26,26,26,26};
array <int> test_trialtype_end[40]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};


string Beobachter = parameter_manager.get_string("Passiver Teilnehmer", "Passiv");
string AktiverProband = parameter_manager.get_string("Aktiver Teilnehmer");

output_file protokoll_file = new output_file;
logfile.set_filename(Beobachter + "_" + AktiverProband + "_beob_protokoll_preference.log");
protokoll_file.open(Beobachter + "_" + AktiverProband + "_beob_protokoll_preference" + ".dat");

input_file control_file = new input_file;
bool result = control_file.open(logfile_directory + "" + AktiverProband + "_protokoll_preference.dat", false);

#term.print_line(logfile_directory + AktiverProband + "_protokoll_preference.dat");
if !result then 
		exit("Aktivdatei " + AktiverProband + "_protokoll_preference.dat nicht gefunden");
end;

protokoll_file.print("j");
protokoll_file.print("\t");
protokoll_file.print("t");
protokoll_file.print("\t");
protokoll_file.print("tb");
protokoll_file.print("\t");
protokoll_file.print("Mu");
protokoll_file.print("\t");
protokoll_file.print("Rf");
protokoll_file.print("\t");
protokoll_file.print("R");
protokoll_file.print("\t");
protokoll_file.print("ac");
protokoll_file.print("\t");
protokoll_file.print("fb");
protokoll_file.print("\n");


sub bool Odd(int i) begin
	return mod(i, 2) == 1;
end;

sub bool Even(int i) begin
	return mod(i, 2) == 0;
end;


#----------------------------------------------------------------------------------------------
# Instruktion

if presentinstruction==1 then
	loop int t=0; until t==8 #Zahl der trials 
	begin	#loop für Instruktion
		t=t+1;
		instr[t].present();
	end;
end;


	
# Trial-Presentation 
int alletrials=0; #Zähler für alle Trials

int num_learn_sessions=0;
int num_test_sessions=0;

int num_corr_block_p1=0;
int num_corr_block_p2=0;
int num_corr_block_p3=0;

int num_corr_test_block_p1=0;
int num_corr_test_block_p2=0;
int num_corr_test_block_p3=0;

int num_corr_pos=0;
int num_corr_neg=0;

int num_dunkel=0;
int num_hell=0;
int num_ess=0;

string line = "";


array<string> parts[8];
	
loop		
	int j=0; #bestimmt, ob Block mit Feedback oder Test-Bloc
	int m;
	until
	j==numrep
	begin
	j=j+1;
	
	#instr[13].present();
	
	m=response_manager.total_response_count();
	
	last_resp=9;
	last_resp2=9;
	last_rew=99;
	
	patterns.shuffle();
	trialtype_1.shuffle();
	trialtype_2.shuffle();
	
	test_patterns.shuffle();
	test_trialtype.shuffle();
	
	test_patterns_end.shuffle();
	test_trialtype_end.shuffle();
	
	jitter.shuffle();
	
	if j<1 then # Schleife sorgt für Bestimmung von numtrials für Test-Trials
		numtrials_block=numtrials_uebung;
		else
		numtrials_block=numtrials_block_1;
	end;
		
	num_corr_block_p1=0;
	num_corr_block_p2=0;
	num_corr_block_p3=0;

	if j!=0 then
		start_learn_session.present();
	else
		#start_uebung.present();
	end;
	
	loop 	
	
	int z1=0;
	int z2=0;
	int z3=0;

	int trials_block=0;
	until
	trials_block==numtrials_block #Gesamtzahl der trials	
	begin				
		alletrials=alletrials+1;
		 
		trials_block=trials_block+1;
		
		# Reading active file for simulation
		trial passiv_reward;		
		if Odd(j) then
			line = control_file.get_line(); 
			line.split("\t", parts);
			if parts.count() != 9 then
				exit("Aktive Eingabedatei ist unvollständig");
			end;
			loop until  !(parts[8] == "99" || parts[8] == "fb") begin #Header und Übung überlesen
				line = control_file.get_line();
				#term.print_line(line);
				line.split("\t", parts); 
			end;
			term.print_line(line);
			if int(parts[3]) != trials_block then exit("Aktive Eingabedatei ist unvollständig"); end; 
		   patterns[trials_block] = int(parts[4]);
			if patterns[trials_block]==12 then
				trialtype_1[z1+1] = int(parts[5]);
				p1_p2_passiv[trialtype_1[z1+1]].set_duration(int(parts[9]));
				if int(parts[7]) == 1 then
					passiv_reward = feedback_r_dunkel;
				else
					passiv_reward = feedback_f_Esspapier;
				end;
			elseif patterns[trials_block]==34 then
				trialtype_2[z2+1] = int(parts[5]);
				p3_p4_passiv[trialtype_2[z2+1]].set_duration(int(parts[9]));
				if int(parts[7]) == 1 then
					passiv_reward = feedback_r_hell;
				else
					passiv_reward = feedback_f_Esspapier;
				end;
			end;
		end;
		
		# Setzten des inter-Trial-Intervalls
		if trials_block<60 then
		fix_jitter1.set_duration(jitter[trials_block]);
		fix_jitter2.set_duration(jitter[trials_block]); #feedback_r1
		fix_jitter3.set_duration(jitter[trials_block]); #feedback_r2
		fix_jitter4.set_duration(jitter[trials_block]); #feedback_f
		else
		fix_jitter1.set_duration(2000);
		fix_jitter2.set_duration(2000); #feedback_r1
		fix_jitter3.set_duration(2000); #feedback_2
		fix_jitter4.set_duration(2000); #feedback_f
		end;
		
		# loop zur Präsentation der richtigen Daten
			
		m=response_manager.total_response_count(); #Anzahl der responses
		
		choice_fb_r_pic1.set_part(1,black_screen_small);
		choice_fb_r_pic1.set_part(2,black_screen_small);
		choice_fb_r_pic2.set_part(1,black_screen_small);
		choice_fb_r_pic2.set_part(2,black_screen_small);
		
		choice_fb_f_pic.set_part(1,black_screen_small);
		choice_fb_f_pic.set_part(2,black_screen_small);
		
		#resp_pic=1 - r_g
		if patterns[trials_block]==12 then
			z1=z1+1;
			if trialtype_1[z1]==1 then	
			resp_pic=1;
			choice_fb_r_pic1.set_part(1,p1_ch);
			choice_fb_r_pic1.set_part(2,p2);
			choice_fb_r_event1.set_event_code("_p2");
			
			choice_fb_f_pic.set_part(1,p1);
			choice_fb_f_pic.set_part(2,p2_ch);
			choice_fb_f_event.set_event_code("p1_");
			
			elseif trialtype_1[z1]==2 then				
			resp_pic=2;
			choice_fb_r_pic1.set_part(1,p2);
			choice_fb_r_pic1.set_part(2,p1_ch);
			choice_fb_r_event1.set_event_code("p2_");
			
			choice_fb_f_pic.set_part(1,p2_ch);
			choice_fb_f_pic.set_part(2,p1);
			choice_fb_f_event.set_event_code("_p1");
			
			elseif trialtype_1[z1]==3 then				
			resp_pic=1;
			choice_fb_r_pic1.set_part(1,p1);
			choice_fb_r_pic1.set_part(2,p2_ch);
			choice_fb_r_event1.set_event_code("p1_");
			
			choice_fb_f_pic.set_part(1,p1_ch);
			choice_fb_f_pic.set_part(2,p2);
			choice_fb_f_event.set_event_code("p2_");
			
			elseif trialtype_1[z1]==4 then				
			resp_pic=2;
			choice_fb_r_pic1.set_part(1,p2_ch);
			choice_fb_r_pic1.set_part(2,p1);
			choice_fb_r_event1.set_event_code("_p1");
			
			choice_fb_f_pic.set_part(1,p2);
			choice_fb_f_pic.set_part(2,p1_ch);
			choice_fb_f_event.set_event_code("p2_");
			end;
			
			right_pic1.set_part(1, black_screen_small);
			right_pic2.set_part(1, black_screen_small);
			wrong_pic.set_part(1, black_screen_small);
			fix_jitter2.set_deltat(1);
			fix_jitter3.set_deltat(1);
			fix_jitter4.set_deltat(1);
			
			if Odd(j) then  # Passive trials
				right_pic1.set_part(1, dunkel);
				right_pic2.set_part(1, hell);
				wrong_pic.set_part(1, Esspapier);
				fix_jitter2.set_deltat(500);
				fix_jitter3.set_deltat(500);
				fix_jitter4.set_deltat(500);
				
				p1_p2_passiv[trialtype_1[z1]].present();
				passiv_reward.present();
			else			
				p1_p2[trialtype_1[z1]].present();
			
			end;
		elseif patterns[trials_block]==34 then
			z2=z2+1;
			
			if trialtype_2[z2]==1 then				
			resp_pic=1;
			choice_fb_r_pic2.set_part(1,p3_ch);
			choice_fb_r_pic2.set_part(2,p4);
			choice_fb_r_event2.set_event_code("_p4");
			
			choice_fb_f_pic.set_part(1,p3);
			choice_fb_f_pic.set_part(2,p4_ch);
			choice_fb_f_event.set_event_code("p3_");
			
			elseif trialtype_2[z2]==2 then				
			resp_pic=2;
			choice_fb_r_pic2.set_part(1,p4);
			choice_fb_r_pic2.set_part(2,p3_ch);
			choice_fb_r_event2.set_event_code("p4_");
			
			choice_fb_f_pic.set_part(1,p4_ch);
			choice_fb_f_pic.set_part(2,p3);
			choice_fb_f_event.set_event_code("_p3");
			
			
			elseif trialtype_2[z2]==3 then				
			resp_pic=1;
			choice_fb_r_pic2.set_part(1,p3);
			choice_fb_r_pic2.set_part(2,p4_ch);
			choice_fb_r_event2.set_event_code("p3_");
			
			choice_fb_f_pic.set_part(1,p3_ch);
			choice_fb_f_pic.set_part(2,p4);
			choice_fb_f_event.set_event_code("_p4");
			
			elseif trialtype_2[z2]==4 then				
			resp_pic=2;
			choice_fb_r_pic2.set_part(1,p4_ch);
			choice_fb_r_pic2.set_part(2,p3);
			choice_fb_r_event2.set_event_code("_p3");
			
			choice_fb_f_pic.set_part(1,p4);
			choice_fb_f_pic.set_part(2,p3_ch);
			choice_fb_f_event.set_event_code("p4_");
			end;
			
			right_pic1.set_part(1, black_screen_small);
			right_pic2.set_part(1, black_screen_small);
			wrong_pic.set_part(1, black_screen_small);
			fix_jitter2.set_deltat(1);
			fix_jitter3.set_deltat(1);
			fix_jitter4.set_deltat(1);
			
			if Odd(j) then  # Passive trials
				right_pic1.set_part(1, dunkel);
				right_pic2.set_part(1, hell);
				wrong_pic.set_part(1, Esspapier);
				fix_jitter2.set_deltat(500);
				fix_jitter3.set_deltat(500);
				fix_jitter4.set_deltat(500);
			
				p3_p4_passiv[trialtype_2[z2]].present();
				passiv_reward.present();
			else
				p3_p4[trialtype_2[z2]].present();
			end;
			
		end;
			# Informationen werden in Files geschrieben
			
		if j>0 then # wichtig - die Übungstrials werden nicht mitgezählt
			lastcode=stimulus_manager.last_stimulus_data().event_code(); #event code des letzten Bildes
			
			if lastcode.find("dunkel")>0 then
			last_rew=250;
			num_dunkel=num_dunkel+1;
			elseif lastcode.find("hell")>0 then
			last_rew=251;
			num_hell=num_hell+1;
			elseif lastcode.find("Esspapier")>0 then
			last_rew=249;
			num_ess=num_ess+1;
			elseif lastcode.find("no_resp")>0 then
			last_rew=9;
			end;
			
			#last_rew = 252;
		end;
			
			
		if response_manager.total_response_count()>0 then
			if j >=2 && response_manager.last_response_data().button()==4 then
			break;
			end;
			
			if response_manager.total_response_count()>m then
				
				if response_manager.last_response_data().type() == response_hit then
					if patterns[trials_block]==12 then
						if trialtype_1[z1]==1 || trialtype_1[z1]==2 then
							num_corr_block_p1=num_corr_block_p1+1;
							last_resp2=1;
						else
							last_resp2=0;
						end;
					elseif patterns[trials_block]==34 then
						if trialtype_2[z2]==1 || trialtype_2[z2]==2 then
							num_corr_block_p2=num_corr_block_p2+1;
							last_resp2=1;
						else
							last_resp2=0;
						end;
					end;
				else
					if patterns[trials_block]==12 then
						if trialtype_1[z1]==3 || trialtype_1[z1]==4 then
							num_corr_block_p1=num_corr_block_p1+1;
							last_resp2=1;
						else
							last_resp2=0;
						end;
					elseif patterns[trials_block]==34 then
						if trialtype_2[z2]==3 || trialtype_2[z2]==4 then
							num_corr_block_p2=num_corr_block_p2+1;
							last_resp2=1;
						else
							last_resp2=0;
						end;
					end;		
			
					last_resp=response_manager.last_response_data().button();
				end;
			else
				last_resp=9;
			end;
		end;
		
		protokoll_file.print(j);
		protokoll_file.print("\t");
		protokoll_file.print(alletrials);
		protokoll_file.print("\t");
		protokoll_file.print(trials_block);
		protokoll_file.print("\t");
		protokoll_file.print(patterns[trials_block]);
		protokoll_file.print("\t");
		
		if patterns[trials_block]==12 then
			protokoll_file.print(trialtype_1[z1]);
		elseif patterns[trials_block]==34 then
			protokoll_file.print(trialtype_2[z2]);
		end;
		
		protokoll_file.print("\t");
		protokoll_file.print(last_resp);
		protokoll_file.print("\t");
		protokoll_file.print(last_resp2);
		protokoll_file.print("\t");
		protokoll_file.print(last_rew);
		
		protokoll_file.print("\n");
		
	end; #loop end
		
	if j==0 then
		uebung.present();
	else
		if j<10 then
			#pause_trial.present();
			instr[16].present();
		end;
	end;
end;	# end äußerste loop

protokoll_file.print("\n");
protokoll_file.print("\n");
protokoll_file.print("\n");
protokoll_file.print("\n");
			
			
protokoll_file.print("dunkle Schokolade");
protokoll_file.print("\t");			
protokoll_file.print(num_dunkel);
protokoll_file.print("\n");

protokoll_file.print("helle Schokolade");
protokoll_file.print("\t");			
protokoll_file.print(num_hell);
protokoll_file.print("\n");

protokoll_file.print("Esspapier");
protokoll_file.print("\t");			
protokoll_file.print(num_ess);

protokoll_file.close();

schokotext1.set_caption(string(num_dunkel)); schokotext1.redraw();
schokotext2.set_caption(string(num_hell)); schokotext2.redraw();
schokotext3.set_caption(string(num_ess)); schokotext3.redraw();

Report.present();

instr[13].present();